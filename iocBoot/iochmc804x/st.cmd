# This should be a test or example startup script
require hmc804x, dev

# HMC804x
epicsEnvSet("ASYN_PORT", "asynHMC804x")
epicsEnvSet("DEVICE_IP", "172.30.244.177")
epicsEnvSet("P", "utgard:")
epicsEnvSet("R", "hmc8042:")
drvAsynIPPortConfigure("${ASYN_PORT}", "${DEVICE_IP}:5025")

## Load record instances
dbLoadRecords("$(hmc804x_DB)/hmc804x.db", "PORT=${ASYN_PORT},P=${P},R=${R=}")
dbLoadRecords("$(hmc804x_DB)/channel.db", "PORT=${ASYN_PORT},P=${P},R=${R=},CH=1")
dbLoadRecords("$(hmc804x_DB)/channel.db", "PORT=${ASYN_PORT},P=${P},R=${R=},CH=2")

asynSetTraceIOMask("$(ASYN_PORT)", 0, 2)
asynSetTraceMask("$(ASYN_PORT)", 0, 9)

iocInit()
